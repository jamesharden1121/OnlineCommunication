package utils

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"hailiang/common/message"
	"net"
)

func GetRedis() {

}

func ReadPkg(conn net.Conn) (mes message.Message, err error) {
	buf := make([]byte, 1024*4)
	fmt.Println("读取客户端发送的数据...")
	_, err = conn.Read(buf[:4])
	if err != nil {
		return
	}
	var pkgLen uint32
	pkgLen = binary.BigEndian.Uint32(buf[:4])
	n, err := conn.Read(buf[:pkgLen])
	if n != int(pkgLen) || err != nil {
		fmt.Println("conn read fail err=", err)
		return
	}
	err = json.Unmarshal(buf[:pkgLen], &mes) //&mes!!!
	if err != nil {
		fmt.Println("json unmarshal fail err=", err)
		return
	}
	return mes, err
}

func WritePkg(conn net.Conn, bytes []byte) (err error) {
	//发长度过去
	var lenPkg uint32
	lenPkg = uint32(len(bytes))
	var buf [4]byte
	binary.BigEndian.PutUint32(buf[:4], lenPkg)
	n, err := conn.Write(buf[:4])
	if err != nil || n != 4 {
		fmt.Println("conn write fail", err)
		return
	}

	//发内容过去
	n, err = conn.Write(bytes)
	if n != int(lenPkg) || err != nil {
		fmt.Println("conn write fail", err)
		return
	}
	return err
}
