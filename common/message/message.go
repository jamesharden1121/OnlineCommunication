package message

const (
	LoginMesType            = "LoginMes"
	LoginResMesType         = "LoginResMes"
	RegisterMesType         = "RegisterMes"
	SmsMesType              = "SmsMes"
	SmsTransferMesType      = "smsTransferMes"
	RegisterResMesType      = "RegisterResMes"
	NotifyUserStatusMesType = "NotifyUserStatusMes"
)

//用户的状态常量
const (
	UserOnline = iota
	UserOffline
	UserBusyness
)

type Message struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

//定义两个消息
type LoginMes struct {
	UserId   int    `json:"userId"`
	Password string `json:"password"`
	UserName string `json:"userName"`
}

//定义两个消息
type RegisterResMes struct {
	Code  int    `json:"code"` //返回状态码 500 表示用户注册失败 200
	Error string `json:"error"`
}

type RegisterMes struct {
	User User `json:"user"`
}

type LoginResMes struct {
	Code    int    `json:"code"` //返回状态码 500 表示用户未注册 200
	UserIds []int  `json:"userIds"`
	Error   string `json:"error"`
}

type NotifyUserStatusMes struct {
	UserId int `json:"userId"`
	Status int `json:"status"`
}

type SmsMes struct {
	User           //匿名结构体
	Content string `json:"content"`
}

type SmsTransferMes struct {
	User           //匿名结构体
	Content string `json:"content"`
}
