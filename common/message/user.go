package message

type User struct {
	UserId   int    `json:"userId"`
	UserName string `json:"userName"`
	Password string `json:"password"`
	Status   int    `json:"status"`
}
