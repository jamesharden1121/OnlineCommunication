package main

import (
	"fmt"
	"hailiang/common/message"
	"hailiang/server/process"
	"hailiang/server/utils"
	"io"
	"net"
)

type Porcessor struct {
	Conn net.Conn
}

//根据客户端发送的消息种类 来觉得调用哪一个函数来处理
func (this *Porcessor) ServerProcessMes(mess *message.Message) (err error) {
	switch mess.Type {
	case message.LoginMesType:
		//处理登录消息
		//创建一个实例
		up := &process.UserProcess{Conn: this.Conn}
		err = up.ServerProcessLogin(mess)
	case message.RegisterMesType:
		//处理注册消息
		up := &process.UserProcess{Conn: this.Conn}
		err = up.ServerProcessRegister(mess)
	case message.SmsMesType:
		sp := &process.SmsProcess{}
		sp.Transpond(mess)
	default:
		fmt.Println("消息类型不存在,无法处理!")

	}
	return err
}

func (this *Porcessor) Process3() (err error) {
	for {
		tr := &utils.Transfer{
			Conn: this.Conn,
		}
		mes, err := tr.ReadPkg()
		if err != nil {

			if err == io.EOF {
				fmt.Println("客户端退出了!")
				return err
			} else {
				fmt.Println("readPkg fail err=", err)
				return err
			}
		}
		err = this.ServerProcessMes(&mes)
		if err != nil {
			fmt.Println("login process err=", err)
			return err
		}
		fmt.Println("mes=", mes)
	}
}
