package main

import (
	"fmt"
	"hailiang/server/model"
	"net"
	"time"
)

//处理连接的协程
func process2(conn net.Conn) {
	//读取client发送的消息
	//defer conn.Close()
	pro := &Porcessor{Conn: conn}
	err := pro.Process3()
	if err != nil {
		fmt.Println("pro err", err)
		return
	}

}

//初始化userDao
func initUserDao() {
	model.MyUserDao = model.NewUserDao(pool)
}

func main() {
	initPool("0.0.0.0:6379", 16, 0, 300*time.Second)
	initUserDao()
	fmt.Println("server listen 8889")
	lis, err := net.Listen("tcp", "0.0.0.0:8889")
	if err != nil {
		fmt.Println("lsiten err=", err)
		return
	}
	//监听成功
	for {
		fmt.Println("wait for client connect")
		conn, err := lis.Accept()
		if err != nil {
			fmt.Println("conn err=", err)
			return
		}
		//保持网络通信,不能断开连接
		go process2(conn)

	}

	//conn := pool.Get()
	//var str ="{\"userId\":1111,\"userName\":\"111\",\"password\":\"123\"}"
	//_, err := conn.Do("hset", "users", 200, str)
	//fmt.Println(err)
}
