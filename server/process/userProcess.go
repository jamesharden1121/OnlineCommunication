package process

import (
	"encoding/json"
	"fmt"
	"hailiang/common/message"
	"hailiang/server/model"
	"hailiang/server/utils"
	"net"
)

type UserProcess struct {
	//连接
	Conn net.Conn
	//增加一个字段,表示该conn是哪一个用户的
	UserId int
}

//上线通知函数
func (this *UserProcess) NotifyOthersOnlineUser(userId int) {
	onlineUser := userMgr.GetAllOnlineUser()
	for id, up := range onlineUser {
		if id == userId {
			continue
		}
		up.NotifyMeOnline(userId)
	}
}

//通知我上线了,抽离的一个方法
func (this *UserProcess) NotifyMeOnline(i int) {
	var mes message.Message
	mes.Type = message.NotifyUserStatusMesType

	var notfy message.NotifyUserStatusMes
	notfy.UserId = i
	notfy.Status = message.UserOnline
	data, err := json.Marshal(notfy)
	if err != nil {
		fmt.Println("json marshal fail", err)
		return
	}
	mes.Data = string(data)
	data, err = json.Marshal(mes)
	if err != nil {
		fmt.Println("json marshal fail", err)
		return
	}
	tf := &utils.Transfer{Conn: this.Conn}
	err = tf.WritePkg(data)
	if err != nil {
		fmt.Println("WritePkg fail", err)
		return
	}
}

//处理登录请求
func (this *UserProcess) ServerProcessLogin(mess *message.Message) (err error) {
	//反序列化数据
	var loginMes message.LoginMes
	err = json.Unmarshal([]byte(mess.Data), &loginMes)

	var resMes message.Message
	resMes.Type = message.LoginResMesType
	var loginResMes message.LoginResMes
	//验证用户id和密码
	user, err := model.MyUserDao.Login(loginMes.UserId, loginMes.Password)
	if err == nil {
		loginResMes.Code = 200
		//todo:user.userId
		this.UserId = loginMes.UserId
		//登录成功后加入在线的userMes中
		userMgr.AddOnlineUsers(this)
		//通知其他在线用户,我上线了
		this.NotifyOthersOnlineUser(loginMes.UserId)
		for id, _ := range userMgr.onlineUsers {
			loginResMes.UserIds = append(loginResMes.UserIds, id)
		}

	} else {
		if err == model.ERROR_USER_NOTEXISTS {
			loginResMes.Code = 500
			loginResMes.Error = "用户不存在,请注册"
		} else if err == model.ERROR_USER_PWD {
			loginResMes.Code = 500
			loginResMes.Error = "密码不正确,请重新登录"
		} else {
			loginResMes.Code = 505
			loginResMes.Error = "系统内部错误"
		}
	}
	fmt.Println("用户", user)
	//序列化数据
	data, err := json.Marshal(loginResMes)
	resMes.Data = string(data)
	data, err = json.Marshal(resMes)
	//返回登录结果数据给客户端
	tf := &utils.Transfer{
		Conn: this.Conn,
	}
	tf.WritePkg(data)
	return err
}

func (this *UserProcess) ServerProcessRegister(mess *message.Message) (err error) {
	//反序列化数据
	var registerMes message.RegisterMes
	err = json.Unmarshal([]byte(mess.Data), &registerMes)
	if err != nil {
		fmt.Println("json fail", err)
		return
	}

	var resMes message.Message
	resMes.Type = message.RegisterResMesType
	var registerResMes message.RegisterResMes
	err = model.MyUserDao.Register(&registerMes.User)
	if err != nil {
		if err == model.ERROR_USER_EXISTS {
			registerResMes.Code = 505
			registerResMes.Error = model.ERROR_USER_EXISTS.Error()
		} else {
			registerResMes.Code = 500
			registerResMes.Error = model.ERROR_UNKNOW.Error()
		}
	} else {
		registerResMes.Code = 200
	}

	//返回数据给客户端
	//结果data的序列化
	data, err := json.Marshal(registerResMes)
	resMes.Data = string(data)
	//整个结果res的序列化
	data, err = json.Marshal(resMes)
	tf := &utils.Transfer{Conn: this.Conn}
	tf.WritePkg(data)
	return err
}
