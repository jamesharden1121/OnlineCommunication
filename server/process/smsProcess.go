package process

import (
	"encoding/json"
	"fmt"
	"hailiang/common/message"
	"hailiang/server/utils"
	"net"
)

type SmsProcess struct {
}

//转发某用户transpond群发的消息给所有用户
func (this *SmsProcess) Transpond(mes *message.Message) {
	var smsTransferMes message.SmsTransferMes
	err := json.Unmarshal([]byte(mes.Data), &smsTransferMes)
	if err != nil {
		fmt.Println("Transpond smsTransferMes Unmarshal fail", err)
		return
	}
	//消息类型改成转发的类型
	mes.Type = message.SmsTransferMesType
	data, err := json.Marshal(mes)
	if err != nil {
		fmt.Println("Transpond mes Marshal fail", err)
		return
	}
	userId := smsTransferMes.UserId
	for id, up := range userMgr.onlineUsers {
		if id == userId {
			continue
		}
		this.sendMesToEachOther(data, up.Conn)
	}

}

func (this *SmsProcess) sendMesToEachOther(date []byte, conn net.Conn) {
	transfer := &utils.Transfer{Conn: conn}
	err := transfer.WritePkg(date)
	if err != nil {
		fmt.Println("sendMesEachOther fail", err)
		return
	}
}
