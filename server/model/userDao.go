package model

import (
	"encoding/json"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"hailiang/common/message"
)

//在服务器启动时就初始化一个userDao实例
var (
	MyUserDao *UserDao
)

//定义一个userDao
type UserDao struct {
	Pool *redis.Pool
}

func NewUserDao(pool *redis.Pool) (userDao *UserDao) {
	userDao = &UserDao{Pool: pool}
	return userDao
}

//注册
func (this *UserDao) Register(user *message.User) (err error) {
	//从redis连接池中获取一个redis连接
	conn := this.Pool.Get() //延时关闭
	defer conn.Close()
	//判断该用户是否已经注册
	u, _ := this.getUserById(user.UserId, conn)
	//如果redis中查不出数据 则用户还未注册
	if u == nil {
		//序列化user
		userstr, err := json.Marshal(user)
		if err != nil {
			return err
		}
		_, err = conn.Do("hset", "users", user.UserId, string(userstr))
		if err != nil {
			fmt.Println(err)
			err = ERROR_UNKNOW
			return err
		}
	} else {
		err = ERROR_USER_EXISTS
		return err
	}
	return err
}

//登录
func (this *UserDao) Login(userId int, password string) (user *User, err error) {
	conn := this.Pool.Get()
	defer conn.Close()
	user, err = this.getUserById(userId, conn)
	if err != nil {
		return
	}

	if user.Password != password {
		err = ERROR_USER_PWD
		return
	}
	return user, err

}

func (this *UserDao) getUserById(userId int, conn redis.Conn) (user *User, err error) {
	res, err := redis.String(conn.Do("hget", "users", userId))
	if err != nil {
		if err == redis.ErrNil {
			err = ERROR_USER_NOTEXISTS
		}
		return
	}
	err = json.Unmarshal([]byte(res), &user)
	if err != nil {
		fmt.Println("user json Unmarshal fail", err)
		return
	}
	return user, err
}
