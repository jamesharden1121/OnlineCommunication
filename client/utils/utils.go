package utils

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"hailiang/common/message"
	"net"
)

type Transfer struct {
	//连接
	Conn net.Conn
	//缓存
	Buf [1024 * 4]byte
}

func (this *Transfer) ReadPkg() (mes message.Message, err error) {
	//buf := make([]byte, 1024*4)
	_, err = this.Conn.Read(this.Buf[:4])
	if err != nil {
		return
	}
	var pkgLen uint32
	pkgLen = binary.BigEndian.Uint32(this.Buf[:4])
	n, err := this.Conn.Read(this.Buf[:pkgLen])
	if n != int(pkgLen) || err != nil {
		fmt.Println("conn read fail err=", err)
		return
	}
	err = json.Unmarshal(this.Buf[:pkgLen], &mes) //&mes!!!
	if err != nil {
		fmt.Println("json unmarshal fail err=", err)
		return
	}
	return mes, err
}

func (this *Transfer) WritePkg(bytes []byte) (err error) {
	//发长度过去
	var lenPkg uint32
	lenPkg = uint32(len(bytes))
	//var buf [4]byte
	binary.BigEndian.PutUint32(this.Buf[:4], lenPkg)
	n, err := this.Conn.Write(this.Buf[:4])
	if err != nil || n != 4 {
		fmt.Println("conn write fail", err)
		return
	}

	//发内容过去
	n, err = this.Conn.Write(bytes)
	if n != int(lenPkg) || err != nil {
		fmt.Println("conn write fail", err)
		return
	}
	return err
}
