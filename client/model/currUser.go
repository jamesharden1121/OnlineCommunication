package model

import (
	"hailiang/common/message"
	"net"
)

type CurrUser struct {
	Conn         net.Conn //自己的连接
	message.User          //该连接的用户信息
}
