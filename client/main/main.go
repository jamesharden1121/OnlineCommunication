package main

import (
	"fmt"
	"hailiang/client/process"
)

func main() {
	//接收用户的选择
	var key int
	//用户id和密码
	var userId int
	var password string
	var userName string

	for {
		fmt.Println("---------------welcome chatroom---------------")
		fmt.Println("\t\t\t 1 login")
		fmt.Println("\t\t\t 2 register")
		fmt.Println("\t\t\t 3 exit")
		fmt.Println("\t\t\t please chose(1-3):")

		fmt.Scanf("%d\n", &key)
		switch key {
		case 1:
			fmt.Println("login in chatroom")
			fmt.Println("please input your id")
			fmt.Scanf("%d\n", &userId)
			fmt.Println("please input your password")
			fmt.Scanf("%s\n", &password)
			up := &process.UserProcess{}
			up.Login(userId, password)
		case 2:
			fmt.Println("register user")
			fmt.Println("please input your id")
			fmt.Scanf("%d\n", &userId)
			fmt.Println("please input your password")
			fmt.Scanf("%s\n", &password)
			fmt.Println("please input your nickname")
			fmt.Scanf("%s\n", &userName)
			up := &process.UserProcess{}
			up.Register(userId, password, userName)
		case 3:
			fmt.Println("logout")
		default:
			fmt.Println("your input is worry,please try again")
		}
	}

}
