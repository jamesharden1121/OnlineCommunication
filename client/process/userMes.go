package process

import (
	"fmt"
	"hailiang/client/model"
	"hailiang/common/message"
)

//客户端要维护的map
var onlineUsers map[int]*message.User = make(map[int]*message.User, 10)
var CurrUser model.CurrUser //在用户登录成功后,完成对currUser的初始化

//在客户端显示当前在线的用户
func outputOnlineUser() {
	//遍历一把 onlineUsers
	fmt.Println("当前在线用户列表:")
	for id, _ := range onlineUsers {
		//如果不显示自己.
		fmt.Println("用户id:\t", id)
	}
}

//编写一个方法，处理返回的NotifyUserStatusMes
func updateUserStatus(notifyUserStatusMes *message.NotifyUserStatusMes) {

	//适当优化
	user, ok := onlineUsers[notifyUserStatusMes.UserId]
	if !ok { //原来没有
		user = &message.User{
			UserId: notifyUserStatusMes.UserId,
		}
	}
	user.Status = notifyUserStatusMes.Status
	onlineUsers[user.UserId] = user

	outputOnlineUser()
}

//var onlineUsers map[int]*message.User = make(map[int]*message.User, 10)
//
////显示当前在线的用户
//func outputOnlineUser() {
//	fmt.Println("当前在线用户 : ")
//	for id, _ := range onlineUsers {
//		fmt.Println("用户id:\t", id)
//	}
//}
//
////更新状态
//func updateUserStatus(mes *message.NotifyUserStatusMes) {
//	user, ok := onlineUsers[mes.UserId]
//	if !ok {
//		user = &message.User{
//			UserId: mes.UserId,
//		}
//	}
//	user.Status = mes.Status
//	onlineUsers[mes.UserId] = user
//
//	outputOnlineUser()
//}
