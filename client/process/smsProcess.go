package process

import (
	"encoding/json"
	"fmt"
	"hailiang/client/utils"
	"hailiang/common/message"
)

type SmsProcess struct {
}

//发送群聊消息

func (this *SmsProcess) SendGroupMes(content string) (err error) {
	var mes message.Message
	mes.Type = message.SmsMesType

	//创建smsMes的实例
	var smsMes message.SmsMes
	smsMes.Content = content
	smsMes.UserId = CurrUser.UserId
	smsMes.Status = CurrUser.Status

	data, err := json.Marshal(smsMes)
	if err != nil {
		fmt.Println("SendGroupMes smsMes marshal fail", err)
		return
	}
	mes.Data = string(data)
	data, err = json.Marshal(mes)
	if err != nil {
		fmt.Println("SendGroupMes mes marshal fail", err)
		return
	}
	transfer := &utils.Transfer{Conn: CurrUser.Conn}
	err = transfer.WritePkg(data)
	if err != nil {
		fmt.Println("SendGroupMes WritePkg fail", err)
		return
	}
	return
}

//读取别人群发的消息
func ReadGroupMes(mes *message.SmsTransferMes) {
	fmt.Println("接收到用户 : ", mes.UserId, "发送的消息 : ", mes.Content)
}
