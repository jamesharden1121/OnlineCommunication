package process

import (
	"encoding/json"
	"fmt"
	"hailiang/client/utils"
	"hailiang/common/message"
	"net"
	"os"
)

type serverProcess struct {
}

//显示登陆成功后的界面...
func ShowMenu() {
	fmt.Println("----------welcome userName----------")
	fmt.Println("----------1. show online user----------")
	fmt.Println("----------2.send message----------")
	fmt.Println("----------3.message list----------")
	fmt.Println("----------4.exit----------")
	fmt.Println("----------please chose(1-4)----------")
	var key int
	var content string
	smsProcess := &SmsProcess{}
	fmt.Scanf("%d\n", &key)
	switch key {
	case 1:
		outputOnlineUser()
	case 2:
		fmt.Println("send message to people")
		fmt.Scanf("%s\n", &content)
		smsProcess.SendGroupMes(content)
	case 3:
		fmt.Println("message list")
	case 4:
		fmt.Println("exit system")
		os.Exit(0)
	default:
		fmt.Println("your input is worry,try again ")

	}
}

//和服务器端保持通讯
func KeepConnect(conn net.Conn) {
	tf := &utils.Transfer{Conn: conn}
	for {
		mes, err := tf.ReadPkg()
		if err != nil {
			fmt.Println("keepconnect fail err", err)
			return
		}
		switch mes.Type {
		case message.NotifyUserStatusMesType:
			var notify message.NotifyUserStatusMes
			json.Unmarshal([]byte(mes.Data), &notify)
			updateUserStatus(&notify)
		case message.SmsTransferMesType: //接收别人群发的消息
			var smsTransferMes message.SmsTransferMes
			json.Unmarshal([]byte(mes.Data), &smsTransferMes)
			ReadGroupMes(&smsTransferMes)
		default:
			fmt.Println("服务器返回了未知的响应类型 : ", mes)
		}

	}
}
