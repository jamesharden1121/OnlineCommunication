package process

import (
	"encoding/json"
	"fmt"
	"hailiang/client/utils"
	"hailiang/common/message"
	"net"
)

type UserProcess struct {
}

func (this *UserProcess) Register(userId int, password string, userName string) (err error) {

	conn, err := net.Dial("tcp", "localhost:8889")
	if err != nil {
		fmt.Println("conn client err=", err)
		return err
	}
	defer conn.Close()
	//1准备消息发送给server
	var mes message.Message
	mes.Type = message.RegisterMesType
	var registerMes message.RegisterMes
	registerMes.User.UserId = userId
	registerMes.User.Password = password
	registerMes.User.UserName = userName
	//2把registerMes序列化
	data, err := json.Marshal(&registerMes)
	if err != nil {
		fmt.Println("json.Marshal err=", err)
		return err
	}
	mes.Data = string(data)

	data2, err := json.Marshal(mes)
	if err != nil {
		fmt.Println("data2 josn err=", err)
		return err
	}
	tf := &utils.Transfer{Conn: conn}
	//3向服务器发送信息
	err = tf.WritePkg(data2)
	if err != nil {
		fmt.Println("conn WritePkg fail err=", err)
		return err
	}

	//4处理服务器返回的消息
	res, err := tf.ReadPkg()
	var registerResMes message.RegisterResMes
	json.Unmarshal([]byte(res.Data), &registerResMes)
	if registerResMes.Code == 200 {
		fmt.Println("注册成功,请登录")
	} else if registerResMes.Code == 500 {
		fmt.Println("保存注册信息失败")
	} else if registerResMes.Code == 505 {
		fmt.Println("用户已存在,请重新注册")
	}
	return err

}

//写一个函数,完成登录
func (this *UserProcess) Login(userId int, password string) (err error) {

	//fmt.Printf("userId = %d  password= %s\n",userId,password)
	//return nil
	conn, err := net.Dial("tcp", "localhost:8889")
	if err != nil {
		fmt.Println("conn client err=", err)
		return err
	}
	defer conn.Close()
	//准备消息发送给server
	var mes message.Message
	mes.Type = message.LoginMesType
	var loginMes message.LoginMes
	loginMes.UserId = userId
	loginMes.Password = password
	data, err := json.Marshal(loginMes)
	if err != nil {
		fmt.Println("json.Marshal err=", err)
		return err
	}
	mes.Data = string(data)

	data2, err := json.Marshal(mes)
	if err != nil {
		fmt.Println("data2 josn err=", err)
		return err
	}
	tf := &utils.Transfer{Conn: conn}
	//先服务器发送信息
	err = tf.WritePkg(data2)
	if err != nil {
		fmt.Println("conn WritePkg fail err=", err)
		return err
	}

	//处理服务器返回的消息
	res, err := tf.ReadPkg()
	if err != nil {
		fmt.Println("conn ReadPkg fail err=", err)
		return err
	}
	var loginResMes message.LoginResMes
	err = json.Unmarshal([]byte(res.Data), &loginResMes)
	if err != nil {
		fmt.Println("json Unmarshal fail err=", err)
		return err
	}
	if loginResMes.Code == 200 {
		//初始化currUser
		CurrUser.Conn = conn
		CurrUser.UserId = userId
		CurrUser.Status = message.UserOnline
		//fmt.Println("登录成功")
		fmt.Println("当前在线用户列表如下:")
		for _, v := range loginResMes.UserIds {
			//不显示自己在线
			if v == userId {
				continue
			}
			fmt.Printf("用户id:\t%d\n", v)
			user := &message.User{UserId: v}
			onlineUsers[v] = user
		}

		//启一个协程,保持和服务端的连接
		go KeepConnect(conn)
		for {
			ShowMenu()
		}
	} else if loginResMes.Code == 500 {
		fmt.Println(loginResMes.Error)
	}
	return err
}
